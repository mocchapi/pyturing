#!/usr/bin/python3

import time


# every state is a dict that must have at least one value
# the valid value keys are '1', '0' and the blank character which is '-' by default
# those keys must also have a dict with the actions that should be run when that key is read from the tape

# the options are  'next', 'write', 'move' and 'exit'
#	'next' declares what state should be transitioned too
#	e.g. 'next':'end' 
#	if this key is omitted the current state will be repeated

#	'write' will write the value given to the current spot on the tape
#	e.g. 'write': '1' will write '1' to the current spot on the tape
#	if this key is omitted the value on the tape remains unchanged

#	'move' will determine which way the tape reader will move next
#	e.g. 'move': 'L' will move one space to the left (negative on the tape)
#	if this key is omitted the machine will keep moving the same direction it was headed before

#	'exit' is a special key that signifies the end of the program
#	e.g. 'exit': True (note that True is not a string) will stop the running program
#	if omitted 'exit': False will be assumed


# test program that copies the 1s on the left of the tape to the right
# its layed out like this here so its a little easier to see
# its defined later in a single line

# testProgram = {
# 	'start':{
# 		'-':{'next': 'searchLeft',
# 			'write': '0',
# 			'move': 'L'
# 			}
# 	},

# 	'searchLeft':{
# 		'1':{
# 			'next': 'centerFromLeft',
# 			'write': '-',
# 			'move': 'R'
# 			},
# 		'0':{
# 			'next': 'finalize',
# 			'move': 'R',
# 			'write': '1'
# 			},
# 		'-':{
# 			'next': 'searchLeft',
# 			'move': 'L'
# 			}
# 	},

# 	'centerFromLeft':{
# 		'0':{
# 			'next': 'searchRight',
# 			'move': 'R'

# 			},
# 		'-':{
# 			'next': 'centerFromLeft',
# 			'move': 'R'
# 			}

# 	},


# 	'centerFromRight':{
# 		'1':{
# 			'next': 'centerFromRight',
# 			'move': 'L'
# 			},
# 		'0':{
# 			'next': 'searchLeft',
# 			'move': 'L'
# 			},
# 		'-':{
# 			'next': 'centerFromRight',
# 			'move': 'L'
# 			}
# 	},

# 	'searchRight':{
# 		'1':{
# 			'next': 'searchRight',
# 			'move': 'R'
# 			},
# 		'-':{
# 			'next': 'centerFromRight',
# 			'write': '1',
# 			'move': 'L'
# 			}
# 	},

# 	'finalize':{
# 		'-':{
# 			'next': 'finalize',
# 			'move': 'R',
# 			'write': '1'
# 			},

# 		'0':{
# 			'write': ' ',
# 			'exit': True
# 			}

# 	}
# }	




class missingStateError(Exception):
	def __init__(self,missingState):
		super().__init__(f'state "{missingState}" does not exist')

class noInstructionsError(Exception):
	def __init__(self,state):
		super().__init__(f'state "{state}" has no instructions')

class missingInstructionsError(Exception):
	def __init__(self,state,value):
		super().__init__(f'state "{state}" has no instructions for value "{value}"')

class maxCyclesExceededError(Exception):
	def __init__(self,maxcycles):
		super().__init__(f'program cycled more than the specified {maxcycles} times')






class turingMachine():
	def __init__(self, states, startState, tape=[], startPos=0, blankChar = '-', startDirection='R', vocal=True):
		self.states = states
		self.tape = list(tape)
		self.dir = startDirection
		self.pos = startPos
		self.state = startState

		self.blnk = blankChar
		
		self.cycle = 0
		self.exit = False
		self.vocal = vocal

		self.usedStates = []


	def next(self):
		self.bit = self.tape[self.pos]
		if self.bit != '1' and self.bit != '0':
			self.bit = self.blnk
		if self.exit: return self.tape
		if self.vocal: self.prints()
		currstate = self.states.get(self.state,None)
		if self.state not in self.usedStates:
			self.usedStates.append(self.state)
		if currstate == None:
			raise missingStateError(self.state)
		else:
			opts = currstate.get(self.bit,None)

		if len(list(currstate)) == 0:
			raise noInstructionsError(self.state)

		if opts == None:
			raise missingInstructionsError(self.state,self.bit)

		self.tape[self.pos] = opts.get('write',self.bit)
		self.dir = opts.get('move',self.dir)
		if self.dir[0].upper() == 'L':
			self.pos-=1
		else:
			self.pos+=1
		self.state = opts.get('next',self.state)
		self.exit = opts.get('exit',False)
		self.cycle +=1
		if self.pos >= len(self.tape):
			self.tape.append(self.blnk)
		return self.tape

	def complete(self,maxcycles=1000,slowdown=0):
		# keeps running cycles until either self.exit is true or the max cycles is exceeded
		# slowdown serves no purpose, it literally just sleeps for x amount of seconds
		# could be helpful for debugging and also makes it look nicer :>
		starttime = time.time()
		while not self.exit:
			if maxcycles < self.cycle:
				raise maxCyclesExceededError(maxcycles)
			self.next()
			time.sleep(slowdown)
		unusedStates = list(set(list(self.states)) - set(self.usedStates))
		self.prints()
		if self.vocal:
			print('===report===')
			print(f'{self.cycle}/{maxcycles} cycles used')
			print(f'finished in {round(time.time()-starttime,3)}s')
			print(f'{len(unusedStates)} unused states',end='')
			if len(unusedStates) > 0:
				print(':')
				print(',\n'.join(unusedStates))
			else:
				print(' ')
			# print(self.exit)
		return self.tape


	def prints(self):
		# print(half1,half2)
		bitto = '\033[92m'+self.tape[self.pos]+'\033[0m'
		spaces = " "*(4-len(str(self.cycle)))
		tracker = f'Cycle {self.cycle}, {spaces}state "{self.state}"'
		tapemsg = f'| {" | ".join(self.tape[:self.pos])} |<{bitto}>| {" | ".join(self.tape[self.pos+1:])} |'
		print(tapemsg,(' '*(100-len(tapemsg))),'->',tracker)


if __name__ == '__main__':
	testProgram = { 'start':{ '-':{'next': 'searchLeft', 'write': '0', 'move': 'L' } }, 'searchLeft':{ '1':{ 'next': 'centerFromLeft', 'write': '-', 'move': 'R' }, '0':{ 'next': 'finalize', 'move': 'R', 'write': '1' }, '-':{ 'next': 'searchLeft', 'move': 'L' } }, 'centerFromLeft':{ '0':{ 'next': 'searchRight', 'move': 'R' }, '-':{ 'next': 'centerFromLeft', 'move': 'R' } }, 'centerFromRight':{ '1':{ 'next': 'centerFromRight', 'move': 'L' }, '0':{ 'next': 'searchLeft', 'move': 'L' }, '-':{ 'next': 'centerFromRight', 'move': 'L' } }, 'searchRight':{ '1':{ 'next': 'searchRight', 'move': 'R' }, '-':{ 'next': 'centerFromRight', 'write': '1', 'move': 'L' } }, 'finalize':{ '-':{ 'next': 'finalize', 'move': 'R', 'write': '1' }, '0':{ 'write': ' ', 'exit': True } } } 
	turing = turingMachine(testProgram,'start','0000111------',startPos=7)
	turing.complete(maxcycles=1000,slowdown=0)
